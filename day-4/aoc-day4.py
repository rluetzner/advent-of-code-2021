#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input

class number:
    def __init__(self, value):
        self.value = value
        self.ismarked = False

    def mark(self, num):
        if self.value == num:
            self.ismarked = True


class board:
    def __init__(self, numbers):
        self.numbers = [[number(n) for n in x] for x in numbers]
        self.lastnumber = '0'

    def mark(self, num):
        self.lastnumber = num
        for i in range(len(self.numbers)):
            for j in range(len(self.numbers[i])):
                self.numbers[i][j].mark(num)

    def all_marked_in_a_line(self, numbers):
        for i in range(len(numbers)):
            marked = filter(lambda x: x.ismarked, numbers[i])
            count = sum(1 for _ in marked)
            if count == len(numbers[0]):
                return True
        return False

    def transpose_numbers(self):
        transposed = [[0 for x in range(5)] for y in range(5)]
        for j in range(len(self.numbers[0])):
            for i in range(len(self.numbers)):
                transposed[i][j] = self.numbers[j][i]
        return transposed

    def haswon(self):
        if self.all_marked_in_a_line(self.numbers):
            return True
        transposed_numbers = self.transpose_numbers()
        if self.all_marked_in_a_line(transposed_numbers):
            return True
        return False

    def calcscore(self):
        unmarked = 0
        for i in range(len(self.numbers)):
            filt = filter(lambda x: not x.ismarked, self.numbers[i])
            unmarked += sum(int(n.value) for n in filt)
        return int(self.lastnumber) * unmarked


def mark(boards, num):
    # print(num)
    for b in boards:
        b.mark(num)


def checkforwin(boards):
    for b in boards:
        if b.haswon():
            print('We have a winner!')
            return True, b.calcscore()
    return False, 0


def parseboards(lines):
    boards = []
    nums = []
    for l in lines[2:]:
        split = l.split()
        if len(split) == 0:
            continue
        else:
            nums.append(split)
            if len(nums) == 5:
                boards.append(board(nums))
                nums = []
    return boards


def play_to_win(boards, bingonumbers):
    for num in bingonumbers:
        mark(boards, num)
        win, score = checkforwin(boards)
        if win:
            return score


def play_to_lose(boards, bingonumbers):
    for num in bingonumbers:
        mark(boards, num)
        if len(boards) > 1:
            boards = list(filter(lambda x: not x.haswon(), boards))
        else:
            win, score = checkforwin(boards)
            if win:
                return score


if __name__ == '__main__':
    lines = get_input('./input.txt')
    bingonumbers = lines[0].split(',')

    boards = parseboards(lines)
    score = play_to_win(boards, bingonumbers)
    print('Final score for first winner: {}'.format(score))

    # Initialize boards again to detect last winner.
    boards = parseboards(lines)
    score = play_to_lose(boards, bingonumbers)
    print('Final score for last winner: {}'.format(score))
