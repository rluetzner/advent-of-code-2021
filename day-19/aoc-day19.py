#!/usr/bin/env python

import numpy as np
import sys
sys.path.append('..')
from libs.helpers import get_input


# 90° rotations along any axis as matrices
rot_x_90 = np.array([[1, 0, 0], [0, 0, 1], [0, -1, 0]])
rot_y_90 = np.array([[0, 0, -1], [0, 1, 0], [1, 0, 0]])
rot_z_90 = np.array([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])


def rotate(x, y, z, p):
    for _ in range(x):
        p = p@rot_x_90
    for _ in range(y):
        p = p@rot_y_90
    for _ in range(z):
        p = p@rot_z_90
    return p


class scanner:
    def __init__(self, beacons, num):
        self.beacons = [np.array([int(x) for x in b.split(',')])
                        for b in beacons]
        self.position = None
        self.name = f'Scanner {num}'
        self.triangles = self.compute_triangles()

    def compute_triangles(self):
        """This will compute all possible triangles from the known
        beacon positions and return a map.
        The map matches the length of the three sides as a tuple
        (p2 - p1, p3 - p2, p1 - p3) to the related points.
        Due to the way the lengths are ordered we will know exaclty
        which points we are looking at."""
        triangles = {}
        for i in range(len(self.beacons)):
            for j in range(i+1, len(self.beacons)):
                for k in range(j+1, len(self.beacons)):
                    p1 = self.beacons[i]
                    p2 = self.beacons[j]
                    p3 = self.beacons[k]
                    points = (p1, p2, p3)
                    dist_1_2 = np.linalg.norm(p2-p1)
                    dist_2_3 = np.linalg.norm(p3-p2)
                    dist_3_1 = np.linalg.norm(p1-p3)
                    triangles[(dist_1_2, dist_2_3, dist_3_1)] = points
        return triangles

    def find_match(self, reference_triangles):
        local_triangles = self.triangles
        reference_triangles = {t: reference_triangles[t] for t in reference_triangles if t in local_triangles}
        local_triangles = {t: local_triangles[t] for t in local_triangles if t in reference_triangles}
        for triangle in local_triangles:
            # We never actually look at the third length, but
            # matching triangles ensures that matches are most
            # likely unique.
            local_dist_1_2, local_dist_2_3, _ = local_triangles[triangle]
            reference_dist_1_2, reference_dist_2_3, _ = reference_triangles[triangle]
            local_vector = local_dist_2_3 - local_dist_1_2
            reference_vector = reference_dist_2_3 - reference_dist_1_2

            # We now try to rotate the local vector to match with the reference.
            for x in range(4):
                for y in range(4):
                    for z in range(4):
                        vector_rot = rotate(x, y, z, local_vector)
                        if np.array_equal(vector_rot, reference_vector):
                            print(f"possible rot {x},{y},{z}")

                            # We need at least 12 matches to be sure that the rotation
                            # is correct.
                            validated = 0
                            for t in local_triangles:
                                l_dist_1_2, l_dist_2_3, _ = local_triangles[triangle]
                                r_dist_1_2, r_dist_2_3, _ = reference_triangles[triangle]
                                r_vector = r_dist_2_3 - r_dist_1_2
                                if np.array_equal(rotate(x, y, z, l_dist_2_3-l_dist_1_2), r_vector):
                                    validated += 3
                                    if validated >= 12:
                                        # We found enough matches. We can now determine
                                        # the absolute position in relation to scanner 0.
                                        self.position = reference_dist_1_2 - \
                                            rotate(x, y, z, local_dist_1_2)

                                        # Match orientation with scanner 0.
                                        self.fix_orientation((x, y, z))
                                        return True
        return False

    def fix_orientation(self, rot):
        """Rotate all beacon coordinates."""
        x, y, z = rot
        self.beacons = [rotate(x, y, z, b) +
                        self.position for b in self.beacons]
        self.triangles = self.compute_triangles()


def parse(lines):
    scanners = []
    beacons = []
    i = 0
    for l in lines:
        if 'scanner' in l:
            if beacons:
                scanners.append(scanner(beacons, i))
                i += 1
                beacons = []
        elif '' == l:
            continue
        else:
            beacons.append(l)

    scanners.append(scanner(beacons, i))
    return scanners


def solve(scanners):
    unsolved = scanners[1:]
    solved = [scanners[0]]
    scanners[0].position = np.array([0, 0, 0])

    # Remember which scanners we already compared.
    # If they don't match up at once, they never will.
    memo = []
    while unsolved:
        for u in unsolved:
            print(u.name)

            for s in solved:

                # Already checked, go on.
                if (u.name, s.name) in memo:
                    continue

                print(f'match with {s.name}')
                known_triangles = s.triangles
                matched = u.find_match(known_triangles)

                if matched:
                    solved.append(u)
                    unsolved.remove(u)
                    print(f"solved {u.name} {u.position}")
                    break
                memo.append((u.name, s.name))


def count_unique_beacons(scanners):
    beacons = [b for b in scanners[0].beacons]
    for s in scanners[1:]:
        beacons += [b for b in s.beacons]
    return len(set(tuple(b) for b in beacons))

def get_max_manhattan_distance(scanners):
    distances = []
    for i in range(len(scanners)):
        for j in range(1, len(scanners)):
            p1 = scanners[i].position
            p2 = scanners[j].position
            d = abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]) + abs(p1[2]-p2[2])
            distances.append(d)
    return max(distances)

if __name__ == '__main__':
    lines = get_input('./input.txt')
    scanners = parse(lines)

    solve(scanners)

    res = count_unique_beacons(scanners)
    print(res)

    res = get_max_manhattan_distance(scanners)
    print(res)
