#!/usr/bin/env python


def algo(num):
    if int(num[0]) != int(num[13]) + 3:
        return False
    if int(num[1]) != int(num[12]) - 8:
        return False
    if int(num[4]) != int(num[11]) - 1:
        return False
    if int(num[7]) != int(num[10]) + 2:
        return False
    if int(num[8]) != int(num[9]) - 3:
        return False
    if int(num[5]) != int(num[6]) - 7:
        return False
    if int(num[2]) != int(num[3]) - 6:
        return False
    return True


if __name__ == '__main__':
    for i in range(99999999999999, 11111111111111, -1):
        lst = list(str(i))
        if any(c == '0' for c in lst):
            continue
        print(i)
        if algo(lst):
            print(i)
            exit(0)
        
