#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from functools import lru_cache

@lru_cache(maxsize=None)
def add(n1, n2):
    return n1 + n2

@lru_cache(maxsize=None)
def mul(n1, n2):
    return n1 * n2

@lru_cache(maxsize=None)
def div(n1, n2):
    return int(n1 / n2)

@lru_cache(maxsize=None)
def mod(n1, n2):
    return n1 % n2

@lru_cache(maxsize=None)
def eql(n1, n2):
    return 0 if n1 == n2 else 1

def run_instructions(lines, inp, register):
    i = 0


    for l in lines:
        split = l.split()
        c = split[0]
        n1 = register[split[1]]
        n2 = 0
        res = 0
        if len(split) > 2:
            try:
                n2 = int(split[2])
            except:
                n2 = register[split[2]]
        if c == 'add':
            res = add(n1, n2)
        elif c == 'inp':
            res = inp[i]
            i += 1
        elif c == 'mul':
            res = mul(n1, n2)
        elif c == 'div':
            res = div(n1, n2)
        elif c == 'mod':
            res = mod(n1, n2)
        elif c == 'eql':
            res = eql(n1, n2)
        else:
            raise(ValueError(f'Unknown command: {l}'))
        register[split[1]] = res


    return register
    
def find_max(lines, register, states = {}):
    if not lines:
        if register['z'] == 0:
            return (True, '')
        else:
            return (False, '')
    state = tuple([lines[0]] + [register[r] for r in register])
    if state in states:
        return states[state]

    l = lines[0]
    split = l.split()
    c = split[0]
    n1 = register[split[1]]
    n2 = 0
    res = 0
    if len(split) > 2:
        try:
            n2 = int(split[2])
        except:
            n2 = register[split[2]]
    if c == 'inp':
        valid = False
        for i in range(9, 0, -1):
            reg = {r: register[r] for r in register}
            reg[split[1]] = i
            valid, inp = find_max(lines[1:], reg)
            if valid:
                return (valid, f'{i}{inp}')
        return (False, '')
    elif c == 'div':
        if n2 == 0:
            input()
            return (False, '')
        res = int(n1 / n2)
    elif c == 'mod':
        if n2 <= 0 or n1 < 0:
            input()
            return (False, '')
        else:
            res = n1 % n2
    elif c == 'add':
        res = add(n1, n2)
    elif c == 'mul':
        res = mul(n1, n2)
    elif c == 'eql':
        res = eql(n1, n2)
    else:
        raise(ValueError(f'Unknown command: {l}'))

    register[split[1]] = res
    mx = find_max(lines[1:], register)
    states[state] = mx
    return mx


if __name__ == '__main__':
    lines = get_input('./example3.txt')
    inp = [9,27]
    register = {'w': 0, 'x': 0, 'y': 0, 'z': 0}
    res = run_instructions(lines, inp, register)
    print(res)

    lines = get_input('./input.txt')
    register = {'w': 0, 'x': 0, 'y': 0, 'z': 0}
    res = find_max(lines, register)
    print(res)
