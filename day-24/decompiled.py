#!/usr/bin/env python


def algo(next_digit):
    stack = []

    for step_number in range(0, 14):
        w = next_digit()
        # x = stack.peek()
        x = stack[-1] if stack else 0

        match step_number:
            case 0:
                x += 12
            case 1:
                x += 11
            case 2:
                x += 14
            case 3:
                stack.pop()
                x += -6
            case 4:
                x += 15
            case 5:
                x += 12
            case 6:
                stack.pop()
                x += -9
            case 7:
                x += 14
            case 8:
                x += 14
            case 9:
                stack.pop()
                x += -5
            case 10:
                stack.pop()
                x += -9
            case 11:
                stack.pop()
                x += -5
            case 12:
                stack.pop()
                x += -2
            case 13:
                stack.pop()
                x += -7

        if x != w:
            y = w
            
            match step_number:
                case 0:
                    y += 4
                case 1:
                    y += 10
                case 2:
                    y += 12
                case 3:
                    y += 14
                case 4:
                    y += 6
                case 5:
                    y += 16
                case 6:
                    y += 1
                case 7:
                    y += 7
                case 8:
                    y += 8
                case 9:
                    y += 11
                case 10:
                    y += 8
                case 11:
                    y += 3
                case 12:
                    y += 1
                case 13:
                    y += 8

            stack.append(y)



if __name__ == '__main__':
    for i in range(99999999999999, 11111111111111, -1):
        lst = list(str(i))
        if any(c == '0' for c in lst):
            continue
        print(i)
        if algo(lambda: int(lst.pop())):
            print(i)
            exit(0)
        
