#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def translate(l):
    split = l.split()
    c = split[0]
    v1 = split[1]
    v2 = split[2] if len(split) > 2 else ''
    if c == 'add':
        return f'\t{v1} = {v1} + {v2}'
    elif c == 'inp':
        return f'\t{v1} = read()'
    elif c == 'mul':
        return f'\t{v1} = {v1} * {v2}'
    elif c == 'div':
        return f'\t{v1} = {v1} / {v2}'
    elif c == 'mod':
        return f'\t{v1} = {v1} % {v2}'
    elif c == 'eql':
        return f'\t{v1} = 1 if {v1} == {v2} else 0'


if __name__ == '__main__':
    lines = get_input('./input.txt')
    lines = [translate(l) for l in lines]
    for l in lines:
        print(l)
