#!/usr/bin/env python


def algo(read):
    w = x = y = z = 0
    w = read()
    x = 1
    y = w + 4
    z = w + 4
    w = read()
    y = 26
    z = z * y
    y = w + 16
    z = z + y
    w = read()
    y = 26
    z = z * y
    y = w + 14
    z = z + y
    w = read()
    x = x * 0
    x = x + z
    x = x % 26
    z = z / 26
    x = x - 13
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 3
    y = y * x
    z = z + y
    w = read()
    x = 1
    y = 26
    z = z * y
    y = w + 11
    z = z + y
    w = read()
    y = 26
    z = z * y
    y = w + 13
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 7
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 11
    y = y * x
    z = z + y
    w = read()
    x = 1
    y = 26
    z = z * y
    y = w + 7
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 12
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 12
    y = y * x
    z = z + y
    w = read()
    x = 1
    y = 26
    z = z * y
    y = w + 15
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 16
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 13
    y = y * x
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 9
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 1
    y = y * x
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 8
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 15
    y = y * x
    z = z + y
    w = read()
    x = z
    x = x % 26
    z = z / 26
    x = x - 8
    x = 0 if x == w else 1
    y = 25
    y = y * x
    y = y + 1
    z = z * y
    y = w + 4
    y = y * x
    z = z + y
    return z == 0


if __name__ == '__main__':
    for i in range(99999999999999, 11111111111111, -1):
        lst = list(str(i))
        if any(c == '0' for c in lst):
            continue
        print(i)
        if algo(lambda: int(lst.pop())):
            print(i)
            exit(0)
        
