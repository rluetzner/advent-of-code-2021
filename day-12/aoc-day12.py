#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def parse_paths(lines):
    paths = {}
    for l in lines:
        split = l.split('-')
        add_to_paths(paths, split[0],split[1])
    return paths


def add_to_paths(paths, a, b):
    if a not in paths:
        paths[a] = []
    if b not in paths:
        paths[b] = []
    paths[a].append(b)
    paths[b].append(a)


def find_valid_paths(paths,start,visited):
    possible_paths = []
    vc = visited.copy()
    if start.lower() == start:
        vc.append(start)
    for p in paths[start]:
        if p == 'end':
            possible_paths.append([p])
            continue
        if p in visited:
            continue
        subpaths = find_valid_paths(paths, p, vc)
        for path in subpaths:
            possible_paths.append(path)
    return [[start] + p for p in possible_paths]


def find_valid_complex(paths,start,visited):
    possible_paths = []
    vc = visited.copy()
    if start.lower() == start:
        vc.append(start)
        if start not in visited and start != 'start' and start != 'end':
            for p in paths[start]:
                if p in visited or p == 'start':
                    continue
                if p == 'end':
                    continue
                subpaths = find_valid_paths(paths, p, visited)
                for path in subpaths:
                    possible_paths.append(path)
    for p in paths[start]:
        if p == 'end':
            possible_paths.append([p])
            continue
        if p in visited:
            continue
        subpaths = find_valid_complex(paths, p, vc)
        for path in subpaths:
            possible_paths.append(path)
    return [[start] + p for p in possible_paths]


if __name__ == '__main__':
    lines = get_input('./input.txt')

    paths = parse_paths(lines)
    posspaths = find_valid_paths(paths, 'start', [])
    print('Paths: {}'.format(sum(1 for p in posspaths)))

    posspaths = find_valid_complex(paths, 'start', [])
    posspaths = [list(x) for x in set(tuple(y) for y in posspaths)]
    print('Paths complex: {}'.format(sum(1 for p in posspaths)))
