#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


class cube:
    def __init__(self, command, axismap):
        self.command = command
        self.coordinates = axismap


    def intersect(self, c):
        x1, x2 = self.coordinates['x']
        xc1, xc2 = c.coordinates['x']
        y1, y2 = self.coordinates['y']
        yc1, yc2 = c.coordinates['y']
        z1, z2 = self.coordinates['z']
        zc1, zc2 = c.coordinates['z']

        # We're not intersecting.
        if x2 < xc1 or x1 > xc2:
            return [self]
        if y2 < yc1 or y1 > yc2:
            return [self]
        if z2 < zc1 or z1 > zc2:
            return [self]

        slices = []

        # Cube overreaches intersection cube in negative x direction.
        if x1 < xc1:
            m = {'x': (x1, xc1-1), 'y': (y1, y2), 'z': (z1, z2)}
            slices.append(cube(self.command, m))

        # Cube overreaches intersection cube in positive x direction.
        if x2 > xc2:
            m = {'x': (xc2+1, x2), 'y': (y1, y2), 'z': (z1, z2)}
            slices.append(cube(self.command, m))

        # Cube overreaches intersection cube in negative y direction.
        # x axis is clamped, because the above code already handles
        # outer 'boxes' in x direction.
        if y1 < yc1:
            m = {'x': (max(xc1, x1), min(xc2, x2)),
                 'y': (y1, yc1-1), 'z': (z1, z2)}
            slices.append(cube(self.command, m))

        # Cube overreaches intersection cube in positive y direction.
        if y2 > yc2:
            m = {'x': (max(xc1, x1), min(xc2, x2)),
                 'y': (yc2+1, y2), 'z': (z1, z2)}
            slices.append(cube(self.command, m))

        # Cube overreaches intersection cube in negative z direction.
        # x and y axis are clamped as explained above.
        if z1 < zc1:
            m = {'x': (max(xc1, x1), min(xc2, x2)), 'y': (
                max(yc1, y1), min(yc2, y2)), 'z': (z1, zc1-1)}
            slices.append(cube(self.command, m))

        # Cube overreaches intersection cube in positive z direction.
        if z2 > zc2:
            m = {'x': (max(xc1, x1), min(xc2, x2)), 'y': (
                max(yc1, y1), min(yc2, y2)), 'z': (zc2+1, z2)}
            slices.append(cube(self.command, m))
        return slices

    def points_in_cube(self):
        area = 1
        for axis in self.coordinates:
            p1, p2 = self.coordinates[axis]
            area *= (p2-p1+1)
        return area


def parse_cube(line):
    split = line.split()
    command = split[0]
    m = {}
    for c in split[1].split(','):
        s = c.split('=')
        axis = s[0]
        coord = s[1].split('..')
        lower = int(coord[0])
        upper = int(coord[1])
        m[axis] = (lower, upper)
    return cube(command, m)


def process_cube(cubes, cube):
    new_cubes = []
    if cube.command == 'on':
        if not cubes:
            return [cube]

        # Intersect the new 'on' cube with
        # other known cubes to get only the
        # unique part.
        new_cubes = [c for c in cubes]
        intersections = [cube]
        for c in cubes:
            new_intersections = []
            for i in intersections:
                new_intersections += i.intersect(c)
            intersections = new_intersections
        new_cubes += intersections

    else:
        for c in cubes:
            new_cubes += c.intersect(cube)

    return new_cubes


def count_lit_cubes(in_cubes, limit):
    cubes = []
    for c in in_cubes:
        process = True
        if limit:
            for a in c.coordinates:
                p1, p2 = c.coordinates[a]
                if p1 > 50 or p2 < -50:
                    process = False
        if process:
            cubes = process_cube(cubes, c)
    return sum(c.points_in_cube() for c in cubes)


if __name__ == '__main__':
    lines = get_input('./input.txt')
    cubes = [parse_cube(l) for l in lines]

    res = count_lit_cubes(cubes, True)
    print(res)

    res = count_lit_cubes(cubes, False)
    print(res)
