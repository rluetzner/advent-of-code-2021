#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input

class line:
    def __init__(self, str, diagonals):
        self.diagonals = diagonals
        split = str.split('->')
        srcsplit = split[0].split(',')
        self.x1 = int(srcsplit[0])
        self.y1 = int(srcsplit[1])
        destsplit = split[1].split(',')
        self.x2 = int(destsplit[0])
        self.y2 = int(destsplit[1])

    def get_max_x(self):
        return max(self.x1, self.x2)

    def get_max_y(self):
        return max(self.y1, self.y2)

    def get_points(self):
        if not self.diagonals and self.x1 != self.x2 and self.y1 != self.y2:
            return []
        elif self.x1 == self.x2:
            y1, y2 = self.y1, self.y2
            if y1 > y2:
                y1, y2 = y2, y1
            for p in range(y1, y2 + 1):
                yield(self.x1, p)
        elif self.y1 == self.y2:
            x1, x2 = self.x1, self.x2
            if x1 > x2:
                x1, x2 = x2, x1
            for p in range(x1, x2 + 1):
                yield(p, self.y1)
        else:
            yield from self.get_points_in_diagonal()

    def get_points_in_diagonal(self):
        x1, x2 = self.x1, self.x2
        y1, y2 = self.y1, self.y2

        # In the following we'll draw lines from either
        # - the top left to bottom right or
        # - the bottom left to the top right.
        # We can transform the coordinates to match this
        # approach.
        if x1 > x2:
            # Flip both coordinates.
            x1, x2, y1, y2 = x2, x1, y2, y1

        # The lines are always 45 degress, so the
        # difference in x and y direction will always
        # be the same.
        diff = x2 - x1

        # Due to our conversion x1 < x2 is implied.
        if y1 < y2:
            for p in range(diff + 1):
                yield(x1 + p, y1 + p)
        else:
            for p in range(diff + 1):
                yield(x1 + p, y1 - p)


class grid:
    def __init__(self, width, height):
        self.grid = [[0 for _ in range(width)] for _ in range(height)]

    def mark(self, p):
        x, y = p
        self.grid[y][x] += 1

    def print(self):
        for y in range(len(self.grid)):
            for x in range(len(self.grid[y])):
                val = self.grid[y][x]
                if val == 0:
                    print('.', end='')
                else:
                    print(val, end='')
            print('')

    def eval(self):
        danger_points = 0
        for y in range(len(self.grid)):
            for x in range(len(self.grid[y])):
                val = self.grid[y][x]
                if val >= 2:
                    danger_points += 1
        return danger_points


def calc(input, evaluate_diagonals):
    lines = [line(l, evaluate_diagonals) for l in input]
    width = max([x.get_max_x() for x in lines]) + 1
    height = max([y.get_max_y() for y in lines]) + 1
    g = grid(width, height)
    for l in lines:
        for p in l.get_points():
            g.mark(p)
    # Only useful for debug purposes.
    # g.print()
    return g.eval()


if __name__ == '__main__':
    inputlines = get_input('./input.txt')

    print('Only straight lines:')
    danger_points = calc(inputlines, False)
    print('Danger points: {}'.format(danger_points))

    print('All lines:')
    danger_points = calc(inputlines, True)
    print('Danger points: {}'.format(danger_points))
