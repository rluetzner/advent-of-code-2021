#!/usr/bin/env python

from functools import reduce
import sys
sys.path.append('..')
from libs.helpers import get_input


pairs = {'(': ')', '[': ']', '{': '}', '<': '>'}


def get_corrupt_chars(lines):
    for l in lines:
        corrupt, invalid_char = is_corrupt(l)
        if corrupt:
            yield(invalid_char)


def complete_lines(lines):
    for l in lines:
        corrupt, stack = is_corrupt(l)
        if not corrupt:
            stack.reverse()
            yield([pairs[c] for c in stack])


def is_corrupt(line):
    """Returns the invalid closing character if the line is corrupt, otherwise
    the full stack of unterminated characters is returned."""
    stack = []
    for c in line:
        if c in pairs:
            stack.append(c)
        elif pairs[stack[-1]] == c:
            stack.pop()
        else:
            return True, c
    return False, stack


corrupt_scores = {')': 3, ']': 57, '}': 1197, '>': 25137}


def get_corrupt_score(corr_chars):
    return sum([corrupt_scores[c] for c in corr_chars])


completion_scores = {')': 1, ']': 2, '}': 3, '>': 4}


def get_completion_score(lines):
    scores = []
    for l in lines:
        score = reduce(lambda x, y: x * 5 + y,
                       [completion_scores[c] for c in l])
        scores.append(score)
    scores.sort()
    return scores[int(len(scores) / 2)]


if __name__ == '__main__':
    lines = get_input('./input.txt')

    cc = get_corrupt_chars(lines)
    score = get_corrupt_score(cc)
    print('score: {}'.format(score))

    comp_chars = complete_lines(lines)
    score = get_completion_score(comp_chars)
    print('score: {}'.format(score))
