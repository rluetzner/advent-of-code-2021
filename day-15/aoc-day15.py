#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input, parse_grid


class node:
    def __init__(self, pos_x, pos_y, cost):
        self.x = pos_x
        self.y = pos_y
        self.cost = cost
        self.distance = 9999999999

    def update(self, shortest_adjacent_distance):
        dist = self.cost + shortest_adjacent_distance
        if self.distance > dist:
            # We found a shorter distance.
            self.distance = dist


def get_neighbours(grid, x, y):
    nodes = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
    for pos in nodes:
        pos_x, pos_y = pos
        if pos_y in range(len(grid)) and pos_x in range(len(grid[y])):
            yield(grid[pos_y][pos_x])


def dijkstra(grid, start_x, start_y, target_x, target_y):
    grid = [[node(x, y, grid[y][x]) for x in range(len(grid[y]))] for y in range(len(grid))]

    # Initialize.
    start = grid[start_y][start_x]
    start.distance = 0
    queue = [start]
    visited = {}

    # Find shortes path.
    while queue:
        # We later sort the queue to ensure that the last
        # entry always has the shortest distance.
        shortest = queue[-1]
        queue.remove(shortest)
        x = shortest.x
        y = shortest.y

        # Points might be added multiple times to the queue,
        # but we only need to calculate the distance once.
        # After the node runs through the algorithm below, the
        # shortest distance is ensured and we can safely skip
        # processing the node again.
        if (x,y) in visited:
            continue
        visited[x,y] = shortest

        # We're at the finish and we've already verified
        # that this is the shortest distance we can get.
        if x == target_x and y == target_y:
            return shortest.distance

        for n in get_neighbours(grid, x, y):
            # Everything we visited already has the shortest
            # distance verified.
            if n not in visited:
                n.update(shortest.distance)
                queue.append(n)

        # This ensures that the last entry has the shortest
        # distance.
        queue.sort(key = lambda q: -q.distance)
    return visited[(target_x,target_y)].distance


def parse_and_expand_grid(lines):
    """This expands the parsed grid to a 5x5 repetition. For every
    expansion the values increase by one. Whenever they reach 9, the
    value will flip over to 1 in the next iteration."""
    g = []
    for i in range(5):
        for line in lines:
            l = []
            for j in range(5):
                l += [(int(c)+j+i-1)%9+1 for c in line]
            g.append(l)
    return g


if __name__ == '__main__':
    lines = get_input('./input.txt')

    g = parse_grid(lines)
    path_dangers = dijkstra(g, 0, 0, len(g[0])-1, len(g)-1)
    print('Min danger path: {}'.format(path_dangers))

    g = parse_and_expand_grid(lines)
    path_dangers = dijkstra(g, 0, 0, len(g[0])-1, len(g)-1)
    print('Min danger path: {}'.format(path_dangers))
