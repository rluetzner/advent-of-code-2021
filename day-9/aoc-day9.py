#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from functools import reduce


class grid:
    def __init__(self, numbers):
        self.numbers = numbers

    def get_value_at(self, x, y):
        if x not in range(len(self.numbers)) or y not in range(
                len(self.numbers[0])):
            # Return the highest value possible for edges.
            return 9
        return self.numbers[x][y]

    def find_danger_points(self):
        for x in range(len(self.numbers)):
            for y in range(len(self.numbers[x])):
                curr_value = self.numbers[x][y]
                neighboured_points = [
                    (x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
                neighboured_values = [self.get_value_at(
                    x, y) for (x, y) in neighboured_points]
                smallest_value = True
                for v in neighboured_values:
                    if v <= curr_value:
                        smallest_value = False
                        break
                if smallest_value:
                    yield((x, y))

    def get_danger_level(self):
        points = self.find_danger_points()
        danger_level = 0
        for p in points:
            x, y = p
            danger_level += self.numbers[x][y] + 1
        return danger_level

    def basin(self, x, y):
        if x not in range(len(self.numbers)) or y not in range(
                len(self.numbers[x])):
            return 0
        if self.numbers[x][y] == 9:
            return 0
        if (x, y) in self.visited:
            return 0
        self.visited.append((x, y))
        return 1 + sum([self.basin(x - 1, y), self.basin(x + 1, y),
                        self.basin(x, y - 1), self.basin(x, y + 1)])

    def get_basin_size(self, x, y):
        self.visited = []
        return self.basin(x, y)

    def get_biggest_basins(self, count):
        danger_points = self.find_danger_points()
        basin_sizes = [self.get_basin_size(x, y) for (x, y) in danger_points]
        for _ in range(count):
            max_basin_size = max(basin_sizes)
            basin_sizes.remove(max_basin_size)
            yield(max_basin_size)

    def multiply_biggest_basins(self, count):
        biggest_basins = self.get_biggest_basins(count)
        return reduce(lambda x, y: x * y, biggest_basins)


def parse_grid(lines):
    numbers = [[int(n) for n in l] for l in lines]
    return grid(numbers)


if __name__ == '__main__':
    lines = get_input('./input.txt')
    g = parse_grid(lines)

    danger_level = g.get_danger_level()
    print('Danger level: {}'.format(danger_level))

    basin_danger = g.multiply_biggest_basins(3)
    print('Multiplied biggest basins: {}'.format(basin_danger))
