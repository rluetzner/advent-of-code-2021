#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def parse_input(lines):
    return (lines[0], lines[2:])


def add_border(pict, border_size, border_char):
    new_pict = []

    def add_full_lines():
        for _ in range(border_size):
            line = [border_char for _ in range(2 * border_size + len(pict[0]))]
            new_pict.append(line)

    add_full_lines()

    for y in range(len(pict)):
        line = []
        line += [border_char for _ in range(border_size)]
        line += pict[y]
        line += [border_char for _ in range(border_size)]
        new_pict.append(line)

    add_full_lines()
    return new_pict


def remove_border(pict, border_size):
    return [line[border_size:-border_size] for line in pict[border_size:-border_size]]


def enhance(pict, enh_alg):
    border_size = 2
    # We will extend the existing border another two rows and columns.
    # This simulates an infinite board as we now get a maximum of 9 'void'
    # tiles to look at.
    extended_pict = add_border(pict, border_size, pict[0][0])

    # Initialize as empty.
    enhanced_pict = [
        ['.' for _ in range(len(extended_pict[0]))] for _ in range(len(extended_pict))]

    for y in range(1, len(extended_pict) - 1):
        for x in range(1, len(extended_pict[y]) - 1):
            sub_pict = ''
            for i in [-1, 0, 1]:
                sub_pict += ''.join(extended_pict[y + i][x-1:x+2])
            sub_pict = sub_pict.replace('.', '0').replace('#', '1')
            idx = int(sub_pict, 2)
            enhanced_pict[y][x] = enh_alg[idx]

    # Remove a smaller border. The enhancement algorithm will make the original
    # image creep out one pixel in each direction.
    # We will keep a one pixel border of 'void' tiles for repeat steps.
    return remove_border(enhanced_pict, border_size-1)


def print_img(pic):
    for y in range(len(pic)):
        print(''.join(pic[y]))


def count(pict):
    return sum(1 if pict[y][x] == '#' else 0 for x in range(len(pict[0])) for y in range(len(pict)))


def run_enhancement(enh_alg, img, enhancement_steps):
    # Add a 'void' border to the image.
    # This is a necessary preparation step for the enhancement
    # algorithm.
    img = add_border(img, 1, '.')
    for _ in range(0, enhancement_steps):
        img = enhance(img, enh_alg)
    return img

if __name__ == '__main__':
    lines = get_input('./input.txt')

    # Part 1
    enh_alg, img = parse_input(lines)
    img = run_enhancement(enh_alg, img, 2)

    print_img(img)
    res = count(img)
    print(res)

    # Part 2
    enh_alg, img = parse_input(lines)
    img = run_enhancement(enh_alg, img, 50)

    res = count(img)
    print(res)
