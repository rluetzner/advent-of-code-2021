#!/usr/bin/env python

from collections import Counter
import sys
sys.path.append('..')
from libs.helpers import get_input


class dice:
    def __init__(self, inc_func, start):
        self.value = start
        self.inc_func = inc_func
        self.rolled = 0

    def roll(self):
        self.value = self.inc_func(self.value)
        self.rolled += 1
        return self.value


class player:
    def __init__(self, start):
        self.current_position = start
        self.buckets = [0 for _ in range(10)]
        self.score = 0

    def move(self, steps):
        self.current_position += steps
        self.current_position = (self.current_position - 1) % 10 + 1
        if self.current_position == 10:
            self.buckets[0] += 1
        else:
            self.buckets[self.current_position] += 1
        res = sum(i * self.buckets[i] for i in range(10))
        res += 10 * self.buckets[0]
        self.score = res

    def has_won(self, winning_score):
        return self.score >= winning_score

    def copy(self):
        p = player(self.current_position)
        p.buckets = [b for b in self.buckets]
        p.score = self.score
        return p


def get_start_pos(lines):
    return [int(l.split(':')[1].strip()) for l in lines]


def play(starting_pos, dice):
    p1 = player(starting_pos[0])
    p2 = player(starting_pos[1])
    while True:
        for p in [p1, p2]:
            rolls = [dice.roll() for _ in range(3)]
            res = sum(rolls)
            p.move(res)
            if p.score >= 1000:
                for losing_player in [p1, p2]:
                    score = losing_player.score
                    if score < 1000:
                        return score * dice.rolled


def play_dirac(starting_pos):
    players = [player(starting_pos[0]), player(starting_pos[1])]
    possible_outcomes = []
    # Use memoization to handle similar states.
    states = {}

    def dirac_player1(players):
        p0 = players[0]
        p1 = players[1]
        t = (1, p0.current_position, p0.score, p1.current_position, p1.score)
        if t in states:
            return states[t]
        winners = [0, 0]
        for i in possible_outcomes:
            p = players[0].copy()
            p.move(i)
            if p.score >= 21:
                winners[0] += weight[i]
            else:
                res = dirac_player2([p, players[1]])
                winners[0] += weight[i] * res[0]
                winners[1] += weight[i] * res[1]
        states[t] = winners
        return winners

    def dirac_player2(players):
        p0 = players[0]
        p1 = players[1]
        t = (2, p1.current_position, p1.score, p0.current_position, p0.score)
        if t in states:
            return states[t]
        winners = [0, 0]

        for i in possible_outcomes:
            p = players[1].copy()
            p.move(i)
            if p.score >= 21:
                winners[1] += weight[i]
            else:
                res = dirac_player1([players[0], p])
                winners[0] += weight[i] * res[0]
                winners[1] += weight[i] * res[1]
        states[t] = winners
        return winners

    for a in range(1, 4):
        for b in range(1, 4):
            for c in range(1, 4):
                possible_outcomes.append(a+b+c)
    weight = Counter(possible_outcomes)
    possible_outcomes = list(set(possible_outcomes))
    return dirac_player1(players)


if __name__ == '__main__':
    lines = get_input('./input.txt')

    # Part 1
    pos = get_start_pos(lines)
    d = dice(lambda x: x+1 if x < 100 else 1, 0)
    res = play(pos, d)
    print(res)

    # Part 2 - God help me...
    res = play_dirac(pos)
    print(res)
