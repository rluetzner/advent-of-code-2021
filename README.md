# Advent of Code 2021

This repo contains my solutions to the [Advent of Code 2021](https://adventofcode.com/2021).

## Coding diary

### Day 1 - Check for increases

> C#

It was already pretty late, therefore I opted to use C# because it's the language I'm most comfortable with.
After reading the problem I already had a solution in mind. The second part of the problem was something I
struggled with for a while until I looked up a possible solution on StackOverflow. I cleaned it up to use
`foreach` instead of `.GetEnumerator()`.

After submitting the solutions I messed around with Reactive Extensions because I remembered the `.Window()`
function it has. Getting the code right wasn't quite as easy as I had initially thought. Converting between
Observables and Enumerables also makes the code a bit less readable in my opinion. I think I actually prefer
the initial solution I had. You'll find this when you go back in the Git history of this project.

### Day 2 - Moving the sub

> Python

I read the problem sometime around noon and then had to take my little kid to bed for his naptime. He can't
sleep without someone being in the room and I couldn't wait to get coding, so that's how I ended up coding this
in Python on Termux on my Android phone.

I initially just wanted to get my thoughts into code quickly and had planned to clean this up later on a real
computer, but it was actually not that bad. The worst thing when coding on a phone has to be the touch keyboard
which always wants to insert a `.` whenever I indent code. Working with Python this way is hell as you can
imagine. 😅

### Day 3 - Binary inputs, Gamma and Epsilon values

> Python

This time I woke up really early and didn't want to boot up my main computer to not wake the rest of the family.
So I ended up coding this again on my phone. It's become a bit of a fun challenge and I worked on this for around
an hour until I got both parts of the solution right.

I'm starting to really like Python for these quick and dirty coding sessions. I forgot to mention earlier that I'm
basically a Python noob. Until yesterday I mostly looked at the Python documentation and read and changed a few
lines in existing scripts. I'm pretty sure that there are libraries out there that would make the code much easier
to read and maintain, but I'm also happy with what I came up with myself.

### Day 4 - Playin' bingo with a squid

> Python

I was sick. Like really, really, really sick. That's also why the code looks very messy and I didn't come back to
clean it up in the end. The performance is also really bad. There is a noticable delay when the script is crunching
the actual input.

### Day 5 - Thermal lines

> Python

Again with the Python. Yesterday I was getting worried, that these puzzles might get too complicated for the small
screen and touchscreen keyboard, but this is still doable. At least the first part was. I committed the code and
finished the second part on an actual keyboard. 🥳 This is soooooo much faster...

I'm also getting by with Python pretty well. If the challenges become more complicated I might end up switching back
to C# though, just because I don't have enough time between work, family and household to fully dive into a new
programming language. If the puzzles stay at this level though I'm quite happy to improve my Python skills a bit over
the month.

### Day 6 - Exponential lanternfish growth

> Python

I have to admit this was a nicely laid out trap. I blindly ran into it and just put into code whatever instructions
were presented to me. I probably should have guessed right away when the calculation for 80 days took more than a few
seconds that there was something wrong with the code, but I ran it for 256 days anyway and ran my Termux instance out
of memory. The first time I've seen Termux crash on me. 😁

I also must admit that I had to look a solution up to figure this one out. The final solution is pretty elegant, but
it's not mine. I'm halfway caught between being disappointed that I couldn't figure this one out and at the same time
amazed at the simplicity of the solution. I hope I'll be able to remember this if I ever should need something like
this again. Maybe for the next advent of code. 😅

### Day 7 - Positioning crabs

> Python

I finally switched to using vim on my phone. While it becones a bit clunky whenever I try to issue commands, it will
automatically indent my Python code for me. I'm also a moron for not thinking of using tabs earlier instead of
whitespaces while working with nano. Vim is quicker for me anyway as I use that mostly on hradless systems and
have the most important stuff memorized (e.g. moving, copying or deleting lines, navigating quickly by
searching, ...). All of that was slowing me down in nano.

To be honest, I'm still confused why I couldn't get a solution using the average to work. Shouldn't the average
position be something where the overall spread is reduced the most? I also initially implemented `recsum` as a
recursive function (which caused a stack explosion), then as an inline iteration (which I terminated because it
was really slow) and finally using Gauß' sum algorithm which I'm glad I remembered from university. 😁

### Day 8 - Playing mastermind

> Python

I thought this was easy and ran off in a totally wrong direction. A bit of a pitfall was that I assumed the lights
would always be listed in the same order, i.e. if 'cgfed' were three than it would always be listed as 'cgfed'. That
is not the case. The input has to be sorted.

For some reason I initially thought that zero was the only number fully contained in eight. This is stupid, because
any digit is fully contained in the digital eight. 😅 All in all this took me way longer than I expected, probably
around 2h in total.

I switched from my phone to the desktop PC though, so coding was at least tolerable. Still a fun puzzle and I enjoyed
figuring it out.

### Day 9 - Lava tubes

> Python

I solved this purely on a desktop PC. This would have driven me mad on a phone screen. I have to admit that I cheated
again and looked up the solutions for this. I've never had to mess around with BFS or DFS that's probably why the
solution wasn't obvious to me from the start. This is a neat trick, although I'm not quite as happy with the
implementation I came up with in the end.

### Day 10 - Fixing syntax

> Python

I remember a collegue of mine telling me about a solution involving one (or many) state machines. While this sounds
like an interesting solution the first thing I saw when I looked at this puzzle was a simple stack. This works
rather nicely and I got this done very quickly. There might be improvements left, but I'm already pretty pleased
with what I came around with.

### Day 11 - Flashing octopuses

> Python

Shouldn't it be octopi? 😅 No matter. Today I was soooo sick and immensely tired that I seriously considered skipping
today's puzzle. I'm glad that I didn't though and I'm especially glad that part 2 wasn't that much of a brain teaser
and only took me a few minutes to get working with the code I already had.

### Day 12 - Pathfinder

> Python

When I read this for the first time this morning, I was close to not even attempting this. As it turns out the first
part wasn't as complicated as I first thought. The second part took me quite some time, even though I immediately
figured out how to reuse the code for part one. It performs poorly and there's most likely a way better solution and
what I have is in dire need of some cleanup. Nevertheless I'm pleased that I could come up with something myself
without sneaking a peek at other solutions. This was again tediously done on my phone in Termux.

### Day 13 - Folding transparent paper

> Python

I don't know why, but I found this much easier than the puzzles of the last few days. I basically had a solution in
mind as soon as I read the instructions and just coded it. Still a very fun puzzle, especially as the solution to
part two must be evaluated manually by reading the printed result from the terminal output.

### Day 14 - Polymerization

> Python

Wow. Another trap and again I walked into it. I expected part two to be "how long until it stabilizes", but it
looks like it doesn't. Therefore I was very quickly finished with part one and the second part took me much
longer. I had the bucket counting down pretty quickly (I think I even got it right the first time), but I had
trouble figuring out how to count the occurrences of characters correctly. The solution I came up with only
works when the named buckets are in the order they appear in. If they were sorted differently this would be a
huge problem and as of now I have no idea how I would solve that. This is a bit dissatisfying, but I'll take
it for now.

----

Oh my god!!! It's so simple. 😩 Of course the two outermost characters never change as all we do is inserting
new characters in between. I had to look this up though and I'm not quite sure if I would have figured this
out without writing it out somehow.

### Day 15 - Pathfinding and Dijkstra

> Python

W. T. F.

This took me almost the entire day. I did not want to copy an existing solution without understanding it
completely, so I read up on Dijkstra (after seeing it mentioned in the solutions thread on Reddit) and
implemented it myself. It performed absoletely abysmally at first. 😩

My initial implementation also calculated the cost for every point in the grid. I let it run for multiple
hours -- just for fun. In the end it came up with a valid solution for part two. I still don't quite
understand why my implementation performs this poorly. It prints a solution in a few minutes, which is better
than before, but people on the thread were talking about seconds or even milliseconds. It turns out sorting the
queue is much more performant than finding the minimum entry every time. This is probably because most of the
queue is already sorted, so adding a new entry at the correct position is fairly quick. Reverse sorting also
increased the performance slightly. My idea behind this was that *appending* an entry to a list is quicker than
*prepending* (because prepending generates a new list object). If I reverse sort the existing list I get the
nodes with smallest distances at the very end. Assuming that the nodes I currently looked at also have a pretty
short distance to the starting point, the short distances end up close together which should further improve
sorting performance.

Overall I dropped from

- ~2h: calculating everything to
- ~15min: using `min()` instead of `sort()` to
- <2min: using reverse sorting.

I did learn something new however. I do remember seeing Dijkstra before, but I don't think I ever understood it
quite in depth as i did today.


### Day 16 - Package decoding

> Python

Holy shit was this obtuse. In the end the only real problem I had was understanding how to determine the exact
length of a subpackage. This was very fun and I'm happy that I came up with a solution without help.

I'm not quite as happy with the hack I came up with using a `remainder` property on the packages. This should
be the actual length of the parsed package instead, but I found this very hard to determine. Maybe I'll give
this another shot.

### Day 17 - Trajectories

> Python

This was the worst. 😩 I feel so ashamed, that I had to look up the solutions as this is actually a fairly
simple puzzle. I even drew a picture to clear this up and didn't see the easy solution for part one.

Part two is copied from Reddit and reimplemented. The copy is still there, but I managed to fix my
implementation. I initially wrongly reset the x values outside the y `for` loop.

### Day 18 - Shellfish math

> Python

Looks like puzzles with lots of text are actually pretty easy to solve as the description of the
necessary steps is complete, whereas something like "find the optimal path" expects external knowledge (i.e.
pathfinding algorithms).

My implementation was plagued by off-by-one errors, but I'm happy i got this to work without parsing objects.
I'm not sure if that would have been easier though. 😅

Looking at the megathread of solutions on Reddit it seems like a lot of other people kept the string
representation intact.

### Day 19 - Matching beacons

> Python

Today was a slap in the face. I was really hoping for something short or at least easy to implement. I know
roughly what needs to be done.

1. Match at least 12 beacons by their distance.
2. Figure out how the sensor's grid must be rotated so all point to point vectors match up.
3. Fix the sensor's grid orientation.
4. Find out the sensor's location in relation to sensor 0.
5. Count unique beacons by their now known absolute positions (i.e. in sensor 0's grid).

My last few days have been very exhausting and I just don't have the energy to solve this right now. Maybe I'll
be back in a few weeks or so.

----

I actually solved this a few days later and got very excited when I managed to get it to work. This is not really
optimal and clean, but it is my own solution. 😅 I came up with the base rotations (90° along any axis) myself
and was then too lazy to figure out which 24 rotations are unique, so I end up doing a bit too much work.

Still not bad for a few hours in bed on a phone. Honestly the performance isn't half bad either IMO.

### Day 20 - Image enhancer

> Python

Although I realized very quickly what the problem with the infinite grid was it still took me ages to get this
working correctly. Part 1 could be kinda brute forced by adding a big enough border initially and removing it
afterwards.

It took me a few hours of downtime to realize that instead of adding an "empty" border I had to replicate an
existing border for the algorithm to work correctly.

Now that I solved it I find the puzzle fun, but I was loudly cursing earlier until I finally realized that the
edges of the grid will start creeping in. I first tried to just cut off these rough edges, but the more correct
(and also more performant) solution is just to keep a small border around and extend it before the next
enhancement step.

### Day 21 - Playing dice in the multiverse

> Python

God help me. My initial implementation took about 10 minutes for part 2, but it got the job done. I looked up the
solutions in the megathread and they used memoization. This amazingly drops the execution time to just a few
seconds.

Part two caught me totally off guard and had me laughing maniacally when I read it. 😅

My code is very complex, because I tried to add memoization to the existing code. The solution I looked at used the
same algorithm and only switched player scores around, which reduces the amount of code and makes caching the state
easier. In my implementation I additionally have to remember whos turn it is.

### Day 22 - Intersecting Conway boxes

> Python

This was fun. The final solution is actually based on the first idea I had when I read the puzzle. Looking at the
entire 3D grid is too much to handle, but we can look at where the boxes from the input intersect and create new
solid boxes from there. Basically, if I hollow out a box, I can split the hollowed out box shape into 6 smaller
non-intersecting solid boxes (i.e. the faces of the box).

I initially gave up on this approach because it got really tedious thinking through all the possible coordinates
(without resorting to a paper drawing). This also took me around 2h to get right. My initial disappointment about
the wrong values quickly vanished when I realized that I had not limited the scope for part one. My hacked together
solution was correct after all!

My naive other approach was of course to use a hashset and compute all possible points, which takes forever when
looking at the entire grid, but can get part one done.
