#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def get_gamma_epsilon_at(lines, idx):
    lines_with_one_at_idx = filter(lambda x: x[idx] == '1', lines)
    ones = sum(1 for _ in lines_with_one_at_idx)
    if ones >= len(lines) / 2:
        return 1, 0
    return 0, 1


def calc(lines):
    gamma = []
    epsilon = []
    # We omit the last character which is a
    # line break.
    for i in range(len(lines[0]) - 1):
        g, e = get_gamma_epsilon_at(lines, i)
        gamma.append(str(g))
        epsilon.append(str(e))
    gamma_str = ''.join(gamma)
    epsilon_str = ''.join(epsilon)
    return gamma_str, epsilon_str


def get_closest_match(lines, use_epsilon):
    matches = lines
    for i in range(len(lines[0])):
        g, e = get_gamma_epsilon_at(matches, i)
        comp = str(e) if use_epsilon else str(g)
        matches = [m for m in matches if m[i] == comp]
        if len(matches) == 1:
            return matches[0]


if __name__ == '__main__':
    lines = get_input('./input.txt')

    gamma_binary, epsilon_binary = calc(lines)
    print('gamma: {}'.format(gamma_binary))
    print('epsilon: {}'.format(epsilon_binary))
    gamma_decimal = int(gamma_binary, 2)
    epsilon_decimal = int(epsilon_binary, 2)
    print('gamma decimal: {}'.format(gamma_decimal))
    print('epsilon decimal: {}'.format(epsilon_decimal))
    print('multiplied: {}'.format(gamma_decimal * epsilon_decimal))

    oxy_binary = get_closest_match(lines, False)
    co2_binary = get_closest_match(lines, True)
    print('oxy: {}'.format(oxy_binary))
    print('co2: {}'.format(co2_binary))
    oxy_decimal = int(oxy_binary, 2)
    co2_decimal = int(co2_binary, 2)
    print('oxy decimal: {}'.format(oxy_decimal))
    print('co2 decimal: {}'.format(co2_decimal))
    print('multiplied: {}'.format(oxy_decimal * co2_decimal))
