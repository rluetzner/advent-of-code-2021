#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def calc(lines, days):
    timers = lines[0].split(',')
    fishes = [0 for _ in range(0, 9)]
    for t in timers:
        fishes[int(t)] += 1
    for t in range(days):
        newfishes = [0 for _ in range(0, 9)]
        for i in range(len(fishes)):
            if i == 0:
                newfishes[6] += fishes[0]
                newfishes[8] += fishes[0]
            else:
                newfishes[i - 1] += fishes[i]
        fishes = newfishes
    return sum([n for n in fishes])


if __name__ == '__main__':
    lines = get_input('./input.txt')

    days = 80
    fishes = calc(lines, days)
    print('fishes: {}'. format(fishes))

    days = 256
    fishes = calc(lines, days)
    print('fishes: {}'. format(fishes))
