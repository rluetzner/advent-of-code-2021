#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def fold(points, fold):
    fold = fold.split()[2]
    fold = fold.split('=')
    horizontal = fold[0] == 'x'
    value = int(fold[1])
    folded = []
    for p in points:
        x,y = p
        if horizontal and x > value:
            diff = x - value
            folded.append(((value - diff), y))
        elif not horizontal and y > value:
            diff = y - value
            folded.append((x, (value - diff)))
        else:
            folded.append((x, y))
    return [f for f in set(tuple(p) for p in folded)]

def print_grid(points):
    width = max([x for x,y in points])+1
    height = max([y for x,y in points])+1
    grid = [['.' for x in range(width)] for y in range(height)]
    for p in points:
        x, y = p
        grid[y][x] = '#'
    for y in range(len(grid)):
        line = ''
        for x in range(len(grid[y])):
            line += grid[y][x]
        print(line)


if __name__ == '__main__':
    lines = get_input('./input.txt')
    points = [(int(p.split(',')[0]), int(p.split(',')[1])) for p in filter(lambda x: len(x) > 0 and 'fold' not in x, lines)]
    
    folds = [f for f in filter(lambda x: len(x) > 0 and 'fold' in x, lines)]
    res = fold(points, folds[0])
    print('Points: {}'.format(sum(1 for _ in res)))

    for f in folds:
        points = fold(points, f)
    print_grid(points)
