#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from math import inf


def parse(lines):
    hallway = [None for c in lines[1].strip('#')]
    val = ['A', 'B', 'C', 'D']
    rooms = [[] for _ in val]
    for l in lines[2:]:
        line = l.replace('#', '').strip()
        if not line:
            continue
        occ = [c for c in line]
        for i in range(len(rooms)):
            rooms[i].append(occ[i])
    rooms = {val[i]: rooms[i] for i in range(len(val))}
    return hallway, rooms


def printstate(hallway, rooms):
    border = ''.join('#' for _ in range(len(hallway)+2))
    print(border)
    hw = ''.join('.' if h == None else h for h in hallway)
    print(f'#{hw}#')
    rm = [rooms[r] for r in rooms]
    for j in range(len(rm[0])):
        occ = '  #'
        for i in range(len(rm)):
            c = rm[i][j]
            c = '.' if c == None else c
            occ += f'{c}#'
        print(occ)
    print('  #########')


doors = {'A': 2, 'B': 4, 'C': 6, 'D': 8}
mult = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}


def solve(hw, rms, states = {}):
    state = [] + hw
    for r in rms:
        state += rms[r]
    state = tuple(state)
    if state in states:
        return states[state]
    hallway = [h for h in hw]
    rooms = {r: [o for o in rms[r]] for r in rms}
    costs = []
    
    # Are we done yet?
    if not any(hallway):
        fin = True
        for c in rooms:
            if not all(o == c for o in rooms[c]):
                fin = False
                break
        if fin:
            # No moves were necessary, but we need to recursively add up.
            print('Finished')
            return [0]

    # Try to move from hallway to room.
    # If possible, this is always the best move.
    for i in range(len(hallway)):
        o = hallway[i]
        if o == None:
            continue
        # Is the room ready?
        if all(x == '.' or x == o for x in rooms[o]):
            door = doors[o]
            hwempty = False
            if door < i:
                hwempty = not any(hallway[door:i])
            else:
                hwempty = not any(hallway[i+1:door])
            if hwempty:
                cost = abs(door - i)
                # Move
                hallway[i] = None
                for j in range(len(rooms[o])):
                    cost += 1
                    if rooms[o][j] == o:
                        rooms[o][j-1] = o
                        cost -= 1
                        break
                    elif j == len(rooms[o])-1:
                        rooms[o][j] = o

                cost *= mult[o]
                printstate(hallway,rooms)
                print('moved home')
                addc = solve(hallway, rooms)
                return [cost+c if c != inf else inf for c in addc]
    print('Nothing todo in hallway')
    # Is this winable?
    canmove = False
    # Try all possible moves out of the rooms.
    for x in rooms:
        if all(o == '.' or o == x for o in rooms[x]):
            continue
        cost = 0
        for i in range(len(rooms[x])):
            o = rooms[x][i]
            cost += 1
            if o != '.':
                pos = hwpos(hallway, x)
                for p in pos:
                    canmove = True
                    hwa = [h for h in hallway]
                    hwa[p] = o
                    ro = {r: [n for n in rooms[r]] for r in rooms}
                    ro[x][i] = '.'
                    printstate(hwa,ro)
                    print('moved to hallway')
                    door = doors[x]
                    finalcost = (cost + abs(door - p)) * mult[o]
                    addc = solve(hwa,ro)
                    costs += [finalcost+c if c != inf else inf for c in addc]
                # Only handle first occupant.
                break
    if not canmove:
        print('dead end')
        costs = [inf]
    states[state] = costs
    return costs

def hwpos(hallway, x):
    door = doors[x]
    poss = []
    for i in range(door-1,-1,-1):
        if hallway[i] == None:
            poss.append(i)
        else:
            break
    for i in range(door+1,len(hallway)):
        if hallway[i] == None:
            poss.append(i)
        else:
            break
    for d in doors:
        if doors[d] in poss:
            poss.remove(doors[d])
    return poss
            

if __name__ == '__main__':
    lines = get_input('./input.txt')
    hallway, rooms = parse(lines)
    printstate(hallway, rooms)
    res = solve(hallway, rooms)
    print(min(res))

    newlines = ['  #D#C#B#A#','  #D#B#A#C#']
    lines = lines[0:3] + newlines + lines[3:]
    hallway, rooms = parse(lines)
    printstate(hallway, rooms)
    res = solve(hallway, rooms)
    print(min(res))
