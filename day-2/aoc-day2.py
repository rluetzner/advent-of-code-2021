#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def evaluate_line(line):
    split = line.split()
    command = split[0]
    value = int(split[1])
    if command == 'forward':
        return value, 0
    if command == 'up':
        return 0, -value
    if command == 'down':
        return 0, value
    raise ValueError('Unknown command: {}'.format(command))


def calc(lines):
    curr_x = 0
    curr_y = 0
    for l in lines:
        x, y = evaluate_line(l)
        curr_x += x
        curr_y += y
    return curr_x, curr_y


def calc_with_aim(lines):
    curr_x = 0
    curr_y = 0
    curr_aim = 0
    for l in lines:
        x, aim = evaluate_line(l)
        curr_x += x
        curr_aim += aim
        curr_y += x * curr_aim
    return curr_x, curr_y


if __name__ == '__main__':
    lines = get_input('./input.txt')

    x, y = calc(lines)
    print('x: {}'.format(x))
    print('y: {}'.format(y))
    print('x*y: {}'.format(x * y))

    x, y = calc_with_aim(lines)
    print('x: {}'.format(x))
    print('y: {}'.format(y))
    print('x*y: {}'.format(x * y))
