#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input

def get_rules(lines):
    rules = {}
    for l in lines:
        split = l.split(' -> ')
        rules[split[0]] = split[1]
    return rules

def do(polymer, rules, steps):
    pb = {}
    for i in range(1, len(polymer)):
        conn = polymer[i-1] + polymer[i]
        if not conn in pb:
            pb[conn] = 0
        pb[conn] += 1
    for i in range(steps):
        pb = polymerize(pb, rules)

    d= {}
    for p in pb:
        c1 = p[0]
        c2 = p[1]
        if not c1 in d:
            d[c1] = 0
        if not c2 in d:
            d[c2] = 0
        d[c2] += pb[p]
    # The left- and rightmost characters will never change. As we're counting all right occurrences
    # above we'll need to add the initial char once.
    d[polymer[0]] += 1
    l = [d[k] for k in d]
    return max(l) - min(l)

def polymerize(polymer, rules):
    newpolymer = {}
    for r in polymer:
        num = polymer[r]
        if r in rules:
            conn1 = r[0] + rules[r]
            conn2 = rules[r] + r[1]
            if not conn1 in newpolymer:
                newpolymer[conn1] = 0
            if not conn2 in newpolymer:
                newpolymer[conn2] = 0
            newpolymer[conn1] += num
            newpolymer[conn2] += num
        else:
            if not r in newpolymer:
                newpolymer[r] = 0
            newpolymer[r] += num
    return newpolymer

if __name__ == '__main__':
    lines = get_input('./input.txt')
    polymer = lines[0]
    rules = get_rules(lines[2:])

    res = do(polymer, rules, 10)
    print('Max - Min: {}'.format(res))
    res = do(polymer, rules, 40)
    print('Max - Min: {}'.format(res))
