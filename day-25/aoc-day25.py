#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def parse(lines):
    return [[c for c in l] for l in lines]


def simulate(grid):
    def normalize(x,y):
        if x == len(grid[0]):
            x = 0
        if y == len(grid):
            y = 0
        return (x,y)

    moved = True
    steps = 0
    while moved:
        moved = False
        steps += 1
        newgrid = [[c for c in l] for l in grid]
        for y in range(len(grid)):
            for x in range(len(grid[y])):
                if grid[y][x] == '>':
                    movx,movy = normalize(x+1, y)
                    if grid[movy][movx] == '.':
                        newgrid[movy][movx] = grid[y][x]
                        newgrid[y][x] = '.'
                        moved = True
        grid = newgrid
        newgrid = [[c for c in l] for l in grid]
        for y in range(len(grid)):
            for x in range(len(grid[y])):
                if grid[y][x] == 'v':
                    movx,movy = normalize(x, y+1)
                    if grid[movy][movx] == '.':
                        newgrid[movy][movx] = grid[y][x]
                        newgrid[y][x] = '.'
                        moved = True
        grid = newgrid
        printgrid(steps, grid)
    return steps


def printgrid(steps, grid):
    print(f'Steps: {steps}')
    for l in grid:
        print(''.join(l))


if __name__ == '__main__':
    lines = get_input('./input.txt')
    grid = parse(lines)
    res = simulate(grid)
    print(res)
