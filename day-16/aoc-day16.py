#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from functools import reduce


class package:
    def __init__(self, package_bin):
        self.version = int(package_bin[0:3], 2)
        self.package_type = int(package_bin[3:6], 2)
        self.numbers = []
        self.subpackages = []
        self.length = 6
        if self.package_type == 4:
            self.parse_literal(package_bin[6:])
        else:
            self.parse_operator(package_bin[6:])


    def parse_literal(self, package):
        if len(package) >= 5:
            self.numbers.append(package[1:5])
            self.length += 5
            if package[0] == '1':
                self.parse_literal(package[5:])


    def parse_operator(self, pkg):
        if pkg[0] == '0':
            total_bits = int(pkg[1:16], 2)
            pkg = pkg[16:]
            # 1 for the subpackage type ID +
            # 15 for the total subpackage lenght.
            self.length += 16
            while total_bits > 0:
                subpackage_length = self.parse_subpackage(pkg)
                pkg = pkg[subpackage_length:]
                total_bits -= subpackage_length
        else:
            number_of_pkgs = int(pkg[1:12], 2)
            pkg = pkg[12:]
            # 1 for the subpackage type ID +
            # 11 for the number of packages.
            self.length += 12
            for _ in range(number_of_pkgs):
                subpackage_length = self.parse_subpackage(pkg)
                pkg = pkg[subpackage_length:]
            self.remainder = pkg


    def parse_subpackage(self, pkg):
        subpackage = package(pkg)
        self.subpackages.append(subpackage)
        self.length += subpackage.length
        return subpackage.length


    def get_version_count(self):
        return self.version + sum(p.get_version_count() for p in self.subpackages)


    def compute(self):
        # Just return the number.
        if self.package_type == 4:
            return self.get_number()

        # Sum or multiply.
        if self.package_type == 0:
            return sum(p.compute() for p in self.subpackages)
        if self.package_type == 1:
            # The initialization with 1 is needed for cases where we only have a
            # single subpackage.
            return reduce(lambda x,y: x * y, [p.compute() for p in self.subpackages], 1)

        # Min or max.
        if self.package_type == 2:
            return min(p.compute() for p in self.subpackages)
        if self.package_type == 3:
            return max(p.compute() for p in self.subpackages)

        # Greater, less or equals
        if self.package_type == 5:
            return 1 if self.subpackages[0].compute() > self.subpackages[1].compute() else 0
        if self.package_type == 6:
            return 1 if self.subpackages[0].compute() < self.subpackages[1].compute() else 0
        if self.package_type == 7:
            return 1 if self.subpackages[0].compute() == self.subpackages[1].compute() else 0
        raise(ValueError(f'Unknown operator type : {self.package_type}'))


    def get_number(self):
        return int(''.join(self.numbers), 2)


def convert_to_binary(line):
    hex = [int(c, 16) for c in line]
    # bin() returns a string which we'll have to pad to
    # get exactly 4 digits.
    binary = [bin(i)[2:].rjust(4, '0') for i in hex]
    return ''.join(binary)


def decrypt_package(pkg):
    return package(pkg)


if __name__ == '__main__':
    lines = get_input('./input.txt')

    packages = []
    for l in lines:
        b = convert_to_binary(l)
        p = decrypt_package(b)
        packages.append(p)
    versions = [p.get_version_count() for p in packages]
    print(f'Final version count: {sum(versions)}')

    res = [p.compute() for p in packages]
    print(f'Final computation result: {res[0]}')
