def get_input(filename):
    with open(filename, 'r') as file:
        return [l.strip() for l in file.readlines()]

def parse_grid(lines):
    return [[int(x) for x in l] for l in lines]
