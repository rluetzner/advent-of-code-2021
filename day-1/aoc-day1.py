#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def calc(lines):
    inc = filter(lambda x: lines[x] > lines[x - 1], range(1, len(lines)))
    return sum(1 for _ in inc)


def calc_with_window(lines):
    windows = [sum([lines[x - 2], lines[x - 1], lines[x]])
               for x in range(2, len(lines))]
    return calc(windows)


if __name__ == '__main__':
    lines = [int(l) for l in get_input('./input.txt')]

    increases = calc(lines)
    print('increases: {}'.format(increases))

    increases = calc_with_window(lines)
    print('increases: {}'.format(increases))
