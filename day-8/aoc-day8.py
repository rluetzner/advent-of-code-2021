#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def find_number_of_easy_digits(lines):
    """Easy digits in this context means digits that can be identified by the
    amount of switched on lights directly."""
    easy_digits = 0
    for l in lines:
        test = l.split('|')[1].split()
        filt = filter(lambda t:
                      len(t) == 2 or
                      len(t) == 3 or
                      len(t) == 4 or
                      len(t) == 7,
                      test)
        easy_digits += sum(1 for _ in filt)
    return easy_digits


def solve(lines):
    decrypted_values = []
    for l in lines:
        split_by_pipe = l.split('|')
        control = split_by_pipe[0].split()
        test = split_by_pipe[1].split()
        control = [''.join(sorted(c)) for c in control]
        test = [''.join(sorted(t)) for t in test]

        numbers = decrypt_numbers(control)
        numbers_dict = {numbers[i]: str(i) for i in range(len(numbers))}

        values = [numbers_dict[t] for t in test]
        decrypted_value = ''.join(values)
        # Debug: Show line results to compare to test input.
        # print(line_result)
        decrypted_values.append(int(decrypted_value))
    return sum(v for v in decrypted_values)


def decrypt_numbers(control):
    """Returns an array where the index corresponds to the input characters used to
    display the number."""
    numbers = ['' for _ in range(10)]
    numbers[1] = find_easy(control, 2)
    numbers[4] = find_easy(control, 4)
    numbers[7] = find_easy(control, 3)
    numbers[8] = find_easy(control, 7)

    numbers[3] = find_subset_of(control, numbers[1], 5)
    numbers[9] = find_subset_of(control, numbers[4], 6)

    control_without_three = list(filter(lambda x: x != numbers[3], control))
    numbers[5] = find_fully_contained_in(control_without_three, numbers[9], 5)

    control_without_nine = list(filter(lambda x: x != numbers[9], control))
    numbers[6] = find_subset_of(control_without_nine, numbers[5], 6)

    numbers[2] = deduce(control, 5, [numbers[3], numbers[5]])
    numbers[0] = deduce(control, 6, [numbers[9], numbers[6]])
    return numbers


def find_easy(control, length):
    """Find numbers that can be deduced from the number of switched on lights."""
    matches = list(filter(lambda x: len(x) == length, control))
    if len(matches) != 1:
        raise ValueError('Something went wrong. Expected a single match.')
    return ''.join(sorted(matches[0]))


def find_subset_of(control, subset, length):
    """This will look for inverted matches, e.g.
    ---                       ---
    | |                         |
    ---    resembles          ---
      |                         |
    ---                       ---."""
    filt = filter(lambda x: len(x) == length, control)
    for f in filt:
        is_subset = True
        for char in subset:
            if char not in f:
                is_subset = False
                break
        if is_subset:
            return ''.join(sorted(f))
    raise ValueError('Something went wrong. No subset match found.')


def find_fully_contained_in(control, comparison, length):
    """This will look for inverted matches, e.g.
    ---                       ---
      |                       | |
    ---    is contained in    ---
      |                         |
    ---                       ---."""
    filt = filter(lambda x: len(x) == length, control)
    for f in filt:
        is_match = True
        for char in f:
            if char not in comparison:
                is_match = False
                break
        if is_match:
            return ''.join(sorted(f))
    raise ValueError('Something went wrong. No match found.')


def deduce(control, length, arr):
    """Return the only number left."""
    matches = list(
        filter(
            lambda x: len(x) == length and x not in arr,
            control))
    if len(matches) != 1:
        raise ValueError('Something went wrong. No deduction possible.')
    return matches[0]


if __name__ == '__main__':
    lines = get_input('./input.txt')

    num_easy_digits = find_number_of_easy_digits(lines)
    print('Easy digits: {}'.format(num_easy_digits))

    total = solve(lines)
    print('Total decrypted value: {}'.format(total))
