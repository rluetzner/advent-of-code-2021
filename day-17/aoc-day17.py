#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def target_bounds(t):
    nums = t.split('=')[1]
    split = nums.split('..')
    return (int(split[0]), int(split[1]))


def getmaxy(target_y_min):
    """The highest possible point has to be something that will
    match the lower bound in y in the next step.
    As the return velocity is exactly the same as the initial
    velocity in y direction, we can just sum this up with
    Gauß."""
    return int((-target_y_min - 1) * -target_y_min / 2)


def simulate(target_x1, target_x2, target_y1, target_y2):
    hits = 0
    # Anything bigger than target_x2 will overshoot the target
    # area.
    for x in range(1, target_x2 + 1):
        # Anything bigger than target_y1 as starting velocity
        # will overshoot the target area.
        for y in range(target_y1, -target_y1):
            velocity_x = x
            velocity_y = y
            pos_x = 0
            pos_y = 0

            while pos_y > target_y1:
                pos_x += velocity_x
                pos_y += velocity_y

                if velocity_x > 0:
                    velocity_x -= 1
                velocity_y -= 1

                # Did we hit the target area?
                if target_y1 <= pos_y <= target_y2 and target_x1 <= pos_x <= target_x2:
                    hits += 1
                    break
    return hits


def solve(x1, x2, y1, y2):
    def tryit(vx, vy):
        x = y = 0
        while y > y1:
            x += vx
            y += vy
            if vx > 0:
                vx -= 1
            vy -= 1
            if x1 <= x <= x2 and y1 <= y <= y2:
                return 1
        return 0
    return sum(tryit(vx, vy) for vx in range(1, x2+1) for vy in range(y1, -y1))


if __name__ == '__main__':
    lines = get_input('./input.txt')
    target = lines[0].split(':')[1].strip()
    split = [t.strip() for t in target.split(',')]
    target_x1, target_x2 = target_bounds(split[0])
    target_y1, target_y2 = target_bounds(split[1])
    maxy = getmaxy(target_y1)
    print(target)
    print(maxy)
    res = simulate(target_x1, target_x2, target_y1, target_y2)
    print(res)
    res = solve(target_x1, target_x2, target_y1, target_y2)
    print(res)
