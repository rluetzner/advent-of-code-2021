#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def recursive_sum(i):
    return int(i * (i + 1) / 2)


def calc(positions, use_recursive_sum):
    min_pos = min(positions)
    max_pos = max(positions)
    possible_positions = [i for i in range(min_pos, max_pos)]
    fuel_costs = [0 for _ in range(len(possible_positions))]
    for i in range(len(possible_positions)):
        if use_recursive_sum:
            fuel_costs[i] = sum(recursive_sum(
                abs(possible_positions[i] - p)) for p in positions)
        else:
            fuel_costs[i] = sum(abs(possible_positions[i] - p)
                                for p in positions)
    return min(fuel_costs)


if __name__ == '__main__':
    lines = get_input('./input.txt')
    positions = [int(p) for p in lines[0].split(',')]

    fuelcost = calc(positions, False)
    print('fuel cost: {}'.format(fuelcost))

    fuelcost = calc(positions, True)
    print('fuel cost with recursive sum: {}'.format(fuelcost))
