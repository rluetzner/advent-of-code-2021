#!/usr/bin/env python


import sys
sys.path.append('..')
from libs.helpers import get_input


def parse_grid(lines):
    return [[int(x) for x in l] for l in lines]


def simulate(grid, steps):
    flashed = 0
    for _ in range(steps):
        increase_all_values(grid)
        flashed += flash(grid)
    return flashed


def synchronize(grid):
    i = 0
    allpos = len(grid) * len(grid[0])
    while True:
        i += 1
        increase_all_values(grid)
        if flash(grid) == allpos:
            return i


def increase_all_values(grid):
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            grid[y][x] += 1


def flash(grid):
    flashed = []
    while contains_ready_to_flash(grid, flashed):
        flash_all(grid, flashed)
    reset_flashed(grid)
    return sum(1 for _ in flashed)


def contains_ready_to_flash(grid, flashed):
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] > 9 and (x, y) not in flashed:
                return True
    return False


def flash_all(grid, flashed):
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] > 9 and (x, y) not in flashed:
                flashed.append((x, y))
                for i in range(x - 1, x + 2):
                    for j in range(y - 1, y + 2):
                        if i == x and j == y:
                            continue
                        increase_at(grid, i, j)


def reset_flashed(grid):
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] > 9:
                grid[y][x] = 0


def increase_at(grid, x, y):
    if y in range(len(grid)) and x in range(len(grid[y])):
        grid[y][x] += 1


if __name__ == '__main__':
    lines = get_input('./input.txt')

    grid = parse_grid(lines)
    steps = 100
    flashed = simulate(grid, steps)
    print('Flashed: {}'.format(flashed))

    grid = parse_grid(lines)
    steps = synchronize(grid)
    print('Synchronized after: {}'.format(steps))
